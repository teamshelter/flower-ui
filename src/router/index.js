import Vue from 'vue'
import Router from 'vue-router'
// import Demo from '@/components/Demo'
import CreateJobSearch from '@/components/CreateJobSearch'
import CreateDriverSearch from '@/components/CreateDriverSearch'
import ListContract from '@/components/ListContract'
import ListJobSearch from '@/components/ListJobSearch'
import ListDriverSearch from '@/components/ListDriverSearch'
import Login from '@/components/Login'
import AdminPanel from '@/components/AdminPanel'
import Main from '@/components/Main'
import CreateContract from '@/components/CreateContract'
import RetrieveContract from '@/components/RetrieveContract'
import RetrieveJobSearch from '@/components/RetrieveJobSearch'
import RetrieveDriverSearch from '@/components/RetrieveDriverSearch'
import RetrieveUser from '@/components/RetrieveUser'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/admin',
      name: 'Admin',
      component: AdminPanel
    },
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/create-job-search',
      name: 'CreateJobSearch',
      component: CreateJobSearch
    },
    {
      path: '/create-driver-search',
      name: 'CreateDriverSearch',
      component: CreateDriverSearch
    },
    {
      path: '/list-job-search',
      name: 'ListJobSearch',
      component: ListJobSearch
    },
    {
      path: '/list-driver-search',
      name: 'ListDriverSearch',
      component: ListDriverSearch
    },
    {
      path: '/retrieve-driver-search/:id',
      name: 'RetrieveDriverSearch',
      component: RetrieveDriverSearch
    },
    {
      path: '/retrieve-job-search/:id',
      name: 'RetrieveJobSearch',
      component: RetrieveJobSearch
    },
    {
      path: '/list-contract',
      name: 'ListContract',
      component: ListContract
    },
    {
      path: '/create-contract/:id',
      name: 'CreateContract',
      component: CreateContract
    },
    {
      path: '/retrieve-contract/:id',
      name: 'RetrieveContract',
      component: RetrieveContract
    },
    {
      path: '/profile',
      name: 'RetrieveUser',
      component: RetrieveUser
    },
    { path: '*', redirect: '/' }
  ]
})
