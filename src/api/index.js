import axios from 'axios'
import * as resource from './resource.js'

export default {
  ListJobSearch: function(options){
    return axios.get(resource.JobSearch, {...options})
  },
  ListDriverSearch: function(options){
    return axios.get(resource.DriverSearch, {...options})
  },
  RetrieveJobSearch: function(options){
    return axios.get(resource.JobSearch + options.params['id'] + '/')
  },
  RetrieveDriverSearch: function(options){
    return axios.get(resource.DriverSearch + options.params['id'] + '/')
  },
  CreateJobSearch: function(options){
    return axios.post(resource.JobSearch, {...options})
  },
  CreateDriverSearch: function(options){
    return axios.post(resource.DriverSearch, {...options})
  },
  UpdateJobSearch: function(options){
    return axios.patch(resource.JobSearch, {...options})
  },
  UpdateDriverSearch: function(options){
    return axios.patch(resource.DriverSearch, {...options})
  },
  DeleteJobSearch: function(options){
    return axios.delete(resource.JobSearch + options['id'] + '/', {...options})
  },
  DeleteDriverSearch: function(options){
    return axios.delete(resource.DriverSearch + options['id'] + '/', {...options})
  },
  ListContract: function(options){
    return axios.get(resource.Contract, {...options})
  },
  RetrieveContract: function(options){
    return axios.get(resource.Contract + options.params['id'] + '/')
  },
  CreateContract: function (options) {
    return axios.post(resource.Contract, {...options})
  },
  UpdateContract: function (options) {
    return axios.put(resource.Contract + options['id'] + '/', {...options})
  },
  DeleteContract: function (options) {
    return axios.delete(resource.Contract + options['id'] + '/', {...options})
  },
  UserList: function(options){
    return axios.get(resource.UserList, {...options})
  },
  UpdateUser: function(options){
    return axios.put(resource.UserList + options['id'] + '/', {...options})
  },
  UserLogin: function (options) {
    return axios.post(resource.UserLogin, {...options})
  },
  UserLogout: function (options) {
    return axios.post(resource.UserLogout, {...options})
  },
  UserAuth: function (options) {
    return axios.get(resource.UserAuth, {...options})
  },
  ChangePassword: function (options) {
    return axios.post(resource.ChangePassword, {...options})
  }
}
