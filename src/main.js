// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './grocery'
import 'bootstrap/dist/js/bootstrap.js'
import 'bootstrap/dist/css/bootstrap.css'
import AxiosDefaults from 'axios/lib/defaults'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faCheckCircle, faTimesCircle, faClock, faFilter} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import 'pretty-checkbox/dist/pretty-checkbox.css';
import PrettyCheckbox from 'pretty-checkbox-vue';
import KeenUI from 'keen-ui';
import 'keen-ui/dist/keen-ui.css';
import VueProgressBar from 'vue-progressbar'


library.add(faCoffee, faCheckCircle, faTimesCircle, faClock, faFilter)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueProgressBar, {
  color: 'rgb(128, 0, 255)',
  failedColor: 'red',
  height: '5px'
})

Vue.use(KeenUI);
Vue.use(PrettyCheckbox)
Vue.config.productionTip = false

AxiosDefaults.xsrfCookieName = "csrftoken"
AxiosDefaults.xsrfHeaderName = "X-CSRFToken"



var moment = require('moment');
Vue.filter('format_date', function (date) {
  if (!date) return '';
  return moment(date, "YYYY-MM-DD HH:mm Z").format('DD.MM.YYYY');
});
Vue.filter('format_date_post', function (date) {
  if (!date) return '';
  return moment(date, "YYYY-MM-DD HH:mm Z").format('YYYY-MM-DD');
});
Vue.filter('format_time', function (date) {
  if (!date) return '';
  return moment(date, "YYYY-MM-DD HH:mm Z").format('HH:mm ');
});
Vue.filter('format_datetime', function (date) {
  if (!date) return '';
  return moment(date, "YYYY-MM-DD HH:mm Z").format('DD.MM.YYYY HH:mm ');
});

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
