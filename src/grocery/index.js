import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLoggedIn : false,
    username: undefined,
    QUOTA_CHOICES: [
      { text: 'Sabahçı', value: 'Sabahçı' },
      { text: 'Gececi', value: 'Gececi' },
      { text: 'Farketmez', value: 'Farketmez' }
    ],
    PERIOD_CHOICES: [
      { text: 'Günlük', value: 'Günlük'},
      { text: 'Devamlı', value: 'Devamlı'},
      { text: 'Belirli bir süre (örnek:30 gün)', value: 'Periyot'},
    ],
    BLOOD_GROUP_CHOICES: [
      { text: 'A Rh Pozitif', value: 'ARh(+)'},
      { text: 'A Rh Negatif', value: 'ARh(-)'},
      { text: 'B Rh Pozitif', value: 'BRh(+)'},
      { text: 'B Rh Negatif', value: 'BRh(-)'},
      { text: 'AB Rh Pozitif', value: 'ABRh(+)'},
      { text: 'AB Rh Negatif', value: 'ABRh(-)'},
      { text: '0 Rh Pozitif', value: '0Rh(+)'},
      { text: '0 Rh Negatif', value: '0Rh(-)'}
    ],
    NON_AUTHORIZED_COMPONENTS: [
      'CreateJobSearch',
      'CreateDriverSearch',
      'RetrieveDriverSearch',
      'RetrieveJobSearch',
      'Main'
    ],
    MENU_OPTIONS: [
      {
        id: 'Main',
        label: 'Ana Menü',
        icon: 'home',
        secondaryText: 'Bir istek oluşturun'
      },
      {
        type: 'divider'
      },
      {
        id: 'ListJobSearch',
        label: 'Taksi Arama',
        icon: 'local_taxi',
        secondaryText: 'Taksi Arama Formlarını Görüntüleyin'
      },
      {
        id: 'ListDriverSearch',
        label: 'Sürücü Arama',
        icon: 'person',
        secondaryText: 'Sürücü Arama Formlarını Görüntüleyin',
      },
      {
        id: 'ListContract',
        label: 'Eşleştirmeler',
        icon: 'code',
        secondaryText: 'Eşleştirmeleri Görüntüleyin'
      },
      {
        type: 'divider'
      },
      {
        id: 'RetrieveUser',
        label: 'Profil',
        icon: 'account_circle',
        secondaryText: 'Profili Görüntüleyin'
      }
    ]
  },
  mutations: {
    LOGIN(state, username) {
      state.isLoggedIn = true;
      state.username = username
    },
    LOGOUT(state) {
      state.isLoggedIn = false;
      state.username = undefined
    },
  },
  actions: {
    Login({ commit }, username) {
      commit('LOGIN', username);
    },
    Logout({ commit }) {
      commit('LOGOUT');
    }

  },
  getters: {
    isLoggedIn: state => {
      return state.isLoggedIn;
    },
    user: state => {
      return state.username;
    },
    quota_choices: state => {
      return state.QUOTA_CHOICES;
    },
    period_choices: state => {
      return state.PERIOD_CHOICES;
    },
    blood_group_choices: state => {
      return state.BLOOD_GROUP_CHOICES
    },
    allowed_components: state => {
      return state.NON_AUTHORIZED_COMPONENTS
    },
    menu_options: state => {
      return state.MENU_OPTIONS
    }
  }
})
