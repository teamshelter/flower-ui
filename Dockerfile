FROM ubuntu:latest
MAINTAINER Şoför Havuzu <mevlanaayas@gmail.com>

USER root

RUN apt-get update
RUN apt-get install -y nginx nodejs
RUN apt-get install vim

# Remove the default Nginx configuration file
RUN rm -v /etc/nginx/nginx.conf

ADD dist /usr/share/nginx/html/
ADD dist /var/www/html/

# Copy a configuration file from the current directory
ADD nginx.conf /etc/nginx/

# Append "daemon off;" to the beginning of the configuration
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Expose ports
EXPOSE 80

# Set the default command to execute
# when creating a new container
CMD ["nginx"]
